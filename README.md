# OpenML dataset: NBA-2k20-player-dataset

https://www.openml.org/d/43420

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
NBA 2k20 analysis.
Content
Detailed attributes for players registered in the NBA2k20.
Acknowledgements
Data scraped from https://hoopshype.com/nba2k/. Additional data about countries and drafts scraped from Wikipedia.
Inspiration
Inspired from this dataset: https://www.kaggle.com/karangadiya/fifa19

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43420) of an [OpenML dataset](https://www.openml.org/d/43420). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43420/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43420/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43420/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

